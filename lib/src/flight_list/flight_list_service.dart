import 'dart:async';

import 'package:angular/core.dart';
import 'flight.dart';
/// Mock service emulating access to a to-do list stored on a server.
@Injectable()
class FlightListService {
  List<Flight> flightsList = <Flight>[];
  
  Future<List<Flight>> getFlightsList() async => flightsList;
}
