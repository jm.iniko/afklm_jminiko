import 'dart:async';

import 'package:angular/angular.dart';
import 'package:angular_components/angular_components.dart';
import 'package:decimal/decimal.dart';
import 'flight_list_service.dart';
import 'flight.dart';
@Component(
  selector: 'flight-list',
  styleUrls: ['flight_list_component.css'],
  templateUrl: 'flight_list_component.html',
  directives: [
    MaterialCheckboxComponent,
    MaterialFabComponent,
    MaterialIconComponent,
    materialInputDirectives,
    NgFor,
    NgIf,
  ],
  providers: [ClassProvider(FlightListService)],
)
class FlightListComponent implements OnInit {
  final FlightListService flightListService;
  List<Flight> flights = [];
  Flight newFlight = new Flight();
  
  FlightListComponent(this.flightListService);

  @override
  Future<Null> ngOnInit() async {
    flights = await flightListService.getFlightsList();
  }

  void add() {
    flights.add(newFlight);
    newFlight = Flight();
  }

  Flight remove(int index) => flights.removeAt(index);
}
